import numpy as np
from sklearn.cluster import KMeans
import cv2
import pytesseract as tess

tess.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

def number_fetch(img):
    value = tess.image_to_string(img, lang='eng', config='--psm 10 --oem 3 digits')
    valid_value = len(value) > 1 and value[0].isdigit()
    if not valid_value:
        return (valid_value, None)
    return(valid_value, value[0])

def skeletonize(img):
    """ OpenCV function to return a skeletonized version of img, a Mat object"""

    #  hat tip to http://felix.abecassis.me/2011/09/opencv-morphological-skeleton/

    img = img.copy() # don't clobber original
    skel = img.copy()

    skel[:,:] = 0
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))

    while True:
        eroded = cv2.morphologyEx(img, cv2.MORPH_ERODE, kernel)
        temp = cv2.morphologyEx(eroded, cv2.MORPH_DILATE, kernel)
        temp  = cv2.subtract(img, temp)
        skel = cv2.bitwise_or(skel, temp)
        img[:,:] = eroded[:,:]
        if cv2.countNonZero(img) == 0:
            break

    return skel

def clustering(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb)

    # channels = cv2.split(image)
    channelIndices = []
    for char in '1':
        channelIndices.append(int(char))
    image = image[:,:,channelIndices]
    if len(image.shape) == 2:
        image.reshape(image.shape[0], image.shape[1], 1)

    # Flatten the 2D image array into an MxN feature vector, where M is
    # the number of pixels and N is the dimension (number of channels).
    reshaped = image.reshape(image.shape[0] * image.shape[1], image.shape[2])

    # Perform K means clustering
    numClusters = 2
    kmeans = KMeans(n_clusters=numClusters, n_init=40, max_iter=500).fit(reshaped)

    # Reshape result back into a 2D array, where each element represents the
    # corresponding pixel's cluster index (0 to K - 1).
    clustering = np.reshape(np.array(kmeans.labels_, dtype=np.uint8),
        (image.shape[0], image.shape[1]))

    # Sort the cluster labels in order of the frequency with which they occur.
    sortedLabels = sorted([n for n in range(numClusters)],
        key=lambda x: -np.sum(clustering == x))

    # Initialize K-means grayscale image; set pixel colors based on clustering.
    kmeansImage = np.zeros(image.shape[:2], dtype=np.uint8)
    for i, label in enumerate(sortedLabels):
        kmeansImage[clustering == label] = int(255 / (numClusters - 1)) * i

    return kmeansImage

def resize_image(image):
    height = int((300 / image.shape[1]) * image.shape[0])
    image = cv2.resize(
        image, 
        (300, height),
        interpolation=cv2.INTER_AREA
    )
    return image

def connect_image(image):
    # give it a blur to connect the dots
    image = cv2.medianBlur(image,15)
    image = cv2.GaussianBlur(image,(3,3),cv2.BORDER_DEFAULT)
    return image

def round_corners(image):
    image = cv2.pyrDown(image)
    __, image = cv2.threshold(image, 200, 250, cv2.THRESH_BINARY)

    # Convert to black and white
    image[image == 255] = 1
    return image

def display_image(orig, image, title):

    concatImage = np.concatenate(
        (
            orig,
            193 * np.ones((orig.shape[0], int(0.0625 * orig.shape[1]), 3), dtype=np.uint8),
            cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
        ), axis=1
    )
    cv2.imshow(title, concatImage)

def determine_number(img_url):
    image = cv2.imread(img_url)
    image = resize_image(image)

    orig = image.copy()

    image = clustering(image)
    image = connect_image(image)
    
    valid_value, value = number_fetch(image)
    if valid_value: return value
    
    image = round_corners(image)

    valid_value, value = number_fetch(image)
    if valid_value: return value

    image = skeletonize(image)
    
    valid_value, value = number_fetch(image)
    if valid_value: return value
    
    return None


determine_number('images/unkown.png')