from selenium import webdriver
from number_recognizer import determine_number
import time

options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.add_argument("--start-maximized")
options.binary_location = "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
driver = webdriver.Chrome(options=options)

driver.get('https://enchroma.com/pages/color-blindness-test#test')
i = 0
while 'get-result?' not in driver.current_url:
    try:
        raw_image = driver.find_element_by_id('main-canvas-container')
        time.sleep(1)
        raw_image.screenshot('scraped_images/' + str(i) + '.png')

        number = determine_number('scraped_images/' + str(i) + '.png')
        print(str(i) + ": " + str(number))

        url = 'javascript:n(0)'
        if number:
            url = 'javascript:n(' + number + ')'
            
        link = driver.find_element_by_xpath('//a[@href="'+url+'"]')
        driver.execute_script("arguments[0].click();", link)
    except Exception:
        break

    i += 1

driver.find_element_by_link_text('No, thanks. Continue to test result.').click()

time.sleep(30)

driver.close()
